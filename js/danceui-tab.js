
//tab
(function ($) {
    var oldFnTab = $.fn.tab;
    $.fn.tab = function (options) {
        options = $.extend({defaultIndex: 0, activeClass: 'daui-tabsnav__item_on', tabsBd: '.J_tabsbd', onToggle: $.noop}, options);
        const $tabsnavItems = this.find('.daui-tabsnav__item');
        const $tabsBdItems = $(options.tabsBd).find('.daui-tabsbd__item');
        //console.log('tabsBdItems:', $tabsBdItems, '长度:', $tabsBdItems.length)
        this.toggle = function (index) {
            const $defaultTabnavItem = $tabsnavItems.eq(index);
            $defaultTabnavItem.addClass(options.activeClass).siblings().removeClass(options.activeClass);
            //console.log('默认打开的tabsBd的index:', index);
            const $defaultTabBdItem = $tabsBdItems.eq(index);
            $defaultTabBdItem.show().siblings().hide();
            options.onToggle(index);
        };
        const self = this;
        this.on('click', '.daui-tabsnav__item', function (e) {
            const index = $(this).index();
            self.toggle(index);
        });
        this.toggle(options.defaultIndex);
        return this;
    };
    $.fn.tab.noConflict = function () {
        return oldFnTab;
    };
})($);