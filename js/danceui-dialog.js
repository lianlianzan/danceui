$('.J_dialogForm').on('click', '.J_dialogSubmit', function(e){
    e.preventDefault();
    var btn = $(this),
        form = btn.parents('form');

	var beforetip = $(this).data('beforetip');
    var isloadding = $(this).data('isloadding');
    var submitdisabled = $(this).data('submitdisabled');
    
    form.ajaxSubmit({
        url: btn.data('action') ? btn.data('action') : form.attr('action'),
        dataType: 'json',
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        beforeSubmit: function (arr, $form, options){
            var text = btn.text();

            if(beforetip){
                layer.msg(beforetip, {icon: 16});
            }
            if(submitdisabled == 1){
                btn.prop('disabled', true);
            }
            if(typeof isloadding == "undefined" || isloadding == 1){
                var index = layer.load(1, {
                    shade: [0.1,'#fff'],
                    time: 1000
                });
            }
        },
        success: function (ret, status, xhr) {
	        layer.closeAll();
            if(ret.code == 1){
				if(ret.msg){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 1, data: ret.data, url: ret.url});
				}
            }else if(ret.code == 0){
				Wei.Util.wapTipNew({msg: ret.msg, icon: 2, data: ret.data, url: ret.url});
            }
        },
    });
});
/**
 * 关闭弹框
 */
$('.J_popup_close').on('click', function(e){
	layer.closeAll();
});
/**
 * 上传按钮美化(弹出框的上传图片问题)
 */
$('.J_filebox').on('click', '.daui-filebox__control', function(){
	$(this).parent().prev('.daui-filebox__file').click();
})
$('.J_filebox').on('change', "input[type='file']", function(){
	console.log('input,file,change');
	var parentObj = $(this).parent();
    var filePath = $(this).val();
    var arr = filePath.split('\\');
    var fileName = arr[arr.length-1];
    parentObj.find('.daui-filebox__name').html(fileName);
    var objUrl = getFileObjectURL(this.files[0]);
    if (objUrl) {
        parentObj.find('.daui-filebox__bd>img').attr("src", objUrl);
        parentObj.find('.daui-filebox__bd>img').fadeIn();
    }
});