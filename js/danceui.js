(function(win) {
	win.Wei = win.Wei || {};
	//Wind全局功能函数命名空间
	Wei.Util = {}
})(window);

if(typeof layerStyle=="undefined"){
	layerStyle = 1;
}

Wei.Util = {
	wapTipNew : function(jsonData, funcafter){
		console.log('layerStyle', layerStyle, ',jsonData:', jsonData);
		if(layerStyle == 1){ //适应电脑的版本
			var options = {
				area: 'auto'
			};
			if(jsonData.icon){
				options.icon = jsonData.icon;
			}
			var data = jsonData.data;
			if(typeof data=="undefined"){
				data = {};
			}
			if(data==null){
				data = {};
			}
			if(data.mode == 'alert'){
				layer.alert(jsonData.msg, options, funcafter);
			}else if(data.mode == 'jumplink'){
				var okName,cancelName;
				if(data.okName){
					okName = data.okName;
				}else{
					okName = '立即跳转';
				}
				if(data.cancelName){
					cancelName = data.cancelName;
				}else{
					cancelName = '留在当页';
				}
				layer.open({
					type: 1
					,title: false //不显示标题栏
					,closeBtn: false
					,area: '300px;'
					,shade: 0.8
					,skin: 'layer-ext-jumplink'
					,resize: false
					,btn: [okName, cancelName]
					,btnAlign: 'c'
					,moveType: 1 //拖拽模式，0或者1
					,content: jsonData.msg
					,success: function(layero){
						var btn = layero.find('.layui-layer-btn');
						btn.find('.layui-layer-btn0').attr({href: data.okLink});
					}
				});
			}else if(data.mode == 'msg'){
				layer.msg(jsonData.msg, options, funcafter);
			}else{
				if(funcafter){
					layer.alert(jsonData.msg, options, funcafter);
				}else{
					layer.alert(jsonData.msg, options);
					if(jsonData.url){
						Wei.Util.readyToJump(jsonData);
					}
				}
			}
		}else if(layerStyle == 2){ //适应触屏的版本
			var options = {
				title: false,
				closeBtn: 1,
				btnAlign: 'c',
				area: 'auto'
			};
			if(jsonData.title){
				options.title = jsonData.title;
			}
			if(jsonData.icon){
				options.icon = jsonData.icon;
			}
			if(jsonData.time){
				options.time = jsonData.time;
			}
			var data = jsonData.data;
			if(typeof data=="undefined"){
				data = {};
			}
			if(data==null){
				data = {};
			}
			if(data.mode == 'alert'){
				options.title = '提示信息';
				if(data.isCallback == 1){
					layer.alert(jsonData.msg, options, funcafter);
				}else{
					layer.alert(jsonData.msg, options);
					Wei.Util.readyToJump(jsonData);
				}
			}else if(data.mode == 'msg'){
				options.skin = 'layer-ext-black';
				if(funcafter){
					layer.msg(jsonData.msg, options, funcafter);
				}else{
					layer.msg(jsonData.msg, options);
					if(jsonData.url){
						Wei.Util.readyToJump(jsonData);
					}
				}
			}else if(data.mode == 'jumplink'){
				var okName,cancelName,skin;
				if(data.okName){
					okName = data.okName;
				}else{
					okName = '立即跳转';
				}
				if(data.cancelName){
					cancelName = data.cancelName;
				}else{
					cancelName = '留在当页';
				}
				if(data.skin){
					skin = data.skin;
				}else{
					skin = 'layer-ext-jumplink';
				}
				layer.open({
					type: 1,
					title: false, //不显示标题栏
					closeBtn: false,
					area: '300px;',
					skin: skin,
					icon: 1,
					resize: false,
					btn: [okName, cancelName],
					btnAlign: 'c',
					moveType: 1, //拖拽模式，0或者1
					content: jsonData.msg,
					success: function(layero){
						var btn = layero.find('.layui-layer-btn');
						btn.find('.layui-layer-btn0').attr({href: data.okLink});
					}
				});
			}else if(data.mode == 'layerConfirm'){
				var okName,cancelName,skin;
				if(data.okName){
					okName = data.okName;
				}else{
					okName = '立即跳转';
				}
				if(data.cancelName){
					cancelName = data.cancelName;
				}else{
					cancelName = '留在当页';
				}
				layer.confirm(jsonData.msg, {
					btn: [okName, cancelName]
					,skin: 'layer-ext-moon'
					,icon: jsonData.icon
				}, function(){
					window.location.href = data.okLink;
				}, function(){
				});
			}else if(data.mode == 'layerAlert'){
				if(data.isCallback == 1){
					layer.alert(jsonData.msg, {skin: 'layer-ext-moon', icon: jsonData.icon}, funcafter);
				}else{
					layer.alert(jsonData.msg, {skin: 'layer-ext-moon', icon: jsonData.icon});
					Wei.Util.readyToJump(jsonData);
				}
			}else if(data.mode == 'layerMsg'){
				if(funcafter){
					layer.msg(jsonData.msg, {icon: jsonData.icon}, funcafter);
				}else{
					layer.msg(jsonData.msg, {icon: jsonData.icon});
					Wei.Util.readyToJump(jsonData);
				}
			}else{
				if(funcafter){
					layer.msg(jsonData.msg, options, funcafter);
				}else{
					layer.msg(jsonData.msg, options);
					Wei.Util.readyToJump(jsonData);
				}
			}
		}else if(layerStyle == 3){
			var data = jsonData.data;
			if(typeof data=="undefined"){
				data = {};
			}
			if(data==null){
				data = {};
			}
			console.log('data:', data)
			if(jsonData.icon == 1){
				layer.open({
					skin: layerSkin,
					type: 1,
					title: '提示',
					shade: [0.8, 'rgba(0,0,0)'],
					content: '<div class="demo-content">'+jsonData.msg+'</div>',
					area: ['80%'],
					success: function(layero){
						console.log('success')
					},
					cancel: function(index, layero){
						console.log('cancel')
						Wei.Util.readyToJump(jsonData);
					}
				});
			}else if(jsonData.icon == 2){
				layer.open({
					skin: layerSkin,
					type: 1,
					title: '提示',
					shade: [0.8, 'rgba(0,0,0)'],
					content: '<div class="demo-content">'+jsonData.msg+'</div>',
					area: ['80%'],
					cancel: function(index, layero){
					}
				});
			}
		}else if(layerStyle == 4){ //信息提示风格
			var data = jsonData.data;
			if(typeof data=="undefined"){
				data = {};
			}
			if(data==null){
				data = {};
			}
			if(data.mode == 'jumplink'){
				var okName,cancelName,skin;
				if(data.okName){
					okName = data.okName;
				}else{
					okName = '立即跳转';
				}
				if(data.cancelName){
					cancelName = data.cancelName;
				}else{
					cancelName = '留在当页';
				}
				layer.open({
					type: 1,
					title: false, //不显示标题栏
					closeBtn: true,
					area: ['300px', '310px'],
					shade: 0.4,
					skin: 'layer-ext-infomation',
					icon: 1,
					resize: false,
					btn: [okName, cancelName],
					btnAlign: 'c',
					moveType: 1, //拖拽模式，0或者1
					content: '<div class="infomation-content">'+jsonData.msg+'</div>',
					success: function(layero){
						var btn = layero.find('.layui-layer-btn');
						btn.find('.layui-layer-btn0').attr({href: data.okLink});
					}
				});
			}else{
				if(jsonData.icon == 1){
					var index = layer.open({
						type: 1,
						title: false, //不显示标题栏
						closeBtn: true,
						area: ['300px', '310px'],
						shade: 0.4,
						skin: 'layer-ext-infomation',
						resize: false,
						moveType: 1, //拖拽模式，0或者1
						content: '<div class="infomation-content">'+jsonData.msg+'</div>',
						success: function(layero){
							//var btn = layero.find('.layui-layer-btn');
							//btn.find('.layui-layer-btn0').attr({href: data.okLink});
						},
						cancel: function(index, layero){
						}
					});
					Wei.Util.readyToJump(jsonData);
				}else if(jsonData.icon == 2){
					okLink = '';
					var index = layer.open({
						type: 1,
						title: false, //不显示标题栏
						closeBtn: true,
						area: ['300px', '310px'],
						shade: 0.4,
						skin: 'layer-ext-infomation layer-ext-infomation-tip',
						resize: false,
						moveType: 1, //拖拽模式，0或者1
						content: '<div class="infomation-content">'+jsonData.msg+'</div>',
						success: function(layero){
							//var btn = layero.find('.layui-layer-btn');
							//btn.find('.layui-layer-btn0').attr({href: data.okLink});
						},
						cancel: function(index, layero){
							//Wei.Util.readyToJump(jsonData);
						}
					});
				}
			}
		}else if(layerStyle == 5){ //自定义
			try{
				var re = customLayer(jsonData);
				//console.log('re', re);
				if(re != true){
					return;
				};
			}catch(err){
				console.log('err:', err);
			}
		}
	},
	readyToJump : function(jsonData){
		console.log('readyToJump,jsonData:', jsonData);
		var jump_time = 1200;
		var data = jsonData.data;
		if(typeof data.jump_type == "undefined"){
			data.jump_type = '';
		}
		if(data.jump_type == 'reload'){
			console.log('reload page');
			setTimeout(function(){
				window.location.reload();
			}, jump_time);
		}else if(jsonData.url){
			if(data.jump_time){
				jump_time = data.jump_time;
			}
			console.log('跳转地址:', jsonData.url);
			setTimeout(function(){
				if(jsonData.url){
					if(window.parent.isDialog){ //iframe弹出页
						window.parent.location.href = jsonData.url;
					}else{
						window.location.href = jsonData.url;
					}
				}
			}, jump_time);
		}
	}
};

// //顶部导航跟随滚动条
// $.fn.smartFloat = function(position_type) {
//	   var position = function(element) {
//		   var top = element.position().top, pos = element.css("position");
//		   $(window).scroll(function() {
//			   var scrolls = $(this).scrollTop();
//			   if (scrolls > top) {
//				   if (window.XMLHttpRequest) {
//					   element.css({
//						   position: "fixed",
//						   top: 0
//					   });
//				   } else {
//					   element.css({
//						   top: scrolls
//					   });
//				   }
//			   }else {
//				   if (typeof position_type != 'undefined') {
//					   element.css({
//						   position: position_type,
//						   top: top
//					   });
//				   } else {
//					   element.css({
//						   position: "static",
//						   top: top
//					   });
//				   }
//			   }
//		   });
//	   };
//	   return $(this).each(function() {
//		   position($(this));
//	   });
// };

//下拉组件
var timer,duration=500,fadeSpeed=50;
$(document).find('.J_dropdown').hover(function(){
	var $this = $(this);
	clearTimeout(timer);
	$this.addClass('hover');
	//$(this).find('ul').css('display', 'block');
	$this.find('ul').fadeIn(fadeSpeed);
// }).on('mouseout', '.J_dropdown', function(){
// 	$(this).removeClass('hover');
// 	$(this).find('ul').css('display', 'none');
// });
},function(){
	var $this = $(this);
	timer = setTimeout(function(){
		$this.removeClass('hover');
		$this.find('ul').fadeOut(fadeSpeed);
	},duration);
});

//所有的ajax form提交,由于大多业务逻辑都是一样的，故统一处理
var ajaxForm_list = $('form.J_ajaxForm');
if (ajaxForm_list.length) {
	$('button.J_submit').on('click', function(e){
		 e.preventDefault();
		 var btn = $(this),
			 form = btn.parents('form.J_ajaxForm');

		 var url = btn.data('action');
		 //if(!url){
			  //alert('缺少data-action');
		 //}
		 form.attr('action', url);
		 form.submit();
	});
	$('.J_ajaxSubmit').on('click', function(e){
		e.preventDefault();
		var btn = $(this),
			form = btn.parents('form.J_ajaxForm');

		var beforetip = $(this).data('beforetip');
		var isloadding = $(this).data('isloadding');
		var submitdisabled = $(this).data('submitdisabled');

		try{
			var re = beforeSubmit();
			//console.log('re', re);
			if(re != true){
				return;
			};
		}catch(err){
			console.log('err:', err);
		}

		form.ajaxSubmit({
			url: btn.data('action') ? btn.data('action') : form.attr('action'), //按钮上是否自定义提交地址(多按钮情况)
			dataType: 'json',
			contentType: "application/x-www-form-urlencoded;charset=utf-8",
			beforeSubmit: function (arr, $form, options){
				var text = btn.text();
				if(beforetip){
					layer.msg(beforetip, {icon: 16});
				}
				if(submitdisabled == 1){
					btn.prop('disabled', true);
				}
				if(typeof isloadding != "undefined"){
					if(isloadding == 1){
						layer.load(0, {shade: [0.1,'#fff'], time: 1000});
					}
					if(isloadding == 2){
						layer.load(1, {shade: [0.1,'#fff'], time: 1000});
					}
					if(isloadding == 3){
						layer.load(2, {shade: [0.1,'#fff'], time: 1000});
					}
					if(isloadding == 4){
						layer.msg('提交中，请稍后', {icon: 16, time: 10000, shade: [0.1, '#000000']});
					}
					if(isloadding == 11){
						layer.load(0, {shade: [0.1,'#fff']});
					}
					if(isloadding == 12){
						layer.load(1, {shade: [0.1,'#fff']});
					}
					if(isloadding == 13){
						layer.load(2, {shade: [0.1,'#fff']});
					}
				}
				return true;
			},
			success: function (ret, statusText, xhr, $form){
				var text = btn.text();
				//console.log(ret);
				//按钮文案、状态修改
				//btn.removeClass('disabled').text(text.replace('中...', '')).parent().find('span').remove();
				//btn.text(text.replace('中...', ''))
				//btn.removeClass('disabled').text(text.replace('中...', ''));
				layer.closeAll();
				if(ret.code == 1){
					if(ret.msg){
						Wei.Util.wapTipNew({msg: ret.msg, icon: 1, data: ret.data, url: ret.url});
					}
					try {
						var re = afterSubmit(ret);
						if(re != true){
							return;
						};
					} catch (err) {
						console.log('err:', err);
					}
				}else if(ret.code == 0){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 2, data: ret.data, url: ret.url});
					btn.prop('disabled', false);
				}else if(ret.status == 'layerConfirm'){
					var extra = ret.extra;
					layer.confirm(ret.msg, {
						btn: [extra.okName, extra.cancelName], //按钮
						shade: false //不显示遮罩
					}, function(){
						if (extra.okUrl){
							window.location.href = extra.okUrl;
						};
					}, function(){
					});
				}
			},
			error: function (ret, statusText, xhr, $form){
				Wei.Util.wapTipNew({msg: 'error! please contact qq:13040', icon: 2, data: {}});
			}
		});
	});
}

//提示后ajax操作(删除功能)
var tList = $('a.J_layerConfirm');
if(tList.length){
	$('a.J_layerConfirm').on('click', function(e){
		e.preventDefault();
		var obj = $(this);
		var url = $(this).attr('href');
		var tip = $(this).data('tip');
		var jump = $(this).data('jump');
		var param = {jump : jump};
		layer.confirm(tip, {
			btn: ['确定','取消'], //按钮
			title: '提示信息',
			shade: false //不显示遮罩
		}, function(){
			layer.load(1, {shade: [0.1,'#fff'],time: 2000});
			layer.closeAll();
			$.get(url, param, function(ret){
				if(ret.code == 1){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 1, data: ret.data, url: ret.url});
				}else if(ret.code == 0){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 2, data: ret.data, url: ret.url});
				}
				try {
					var re = afterConfirm(ret);
					if (re != true) {
						return;
					};
				} catch (err) {

				}
			}, 'json');
		}, function(){
		});
	});
}

//prompt后ajax操作(删除功能)
var tList = $('a.J_layerPrompt');
if(tList.length){
	$('a.J_layerPrompt').on('click', function(e){
		e.preventDefault();
		var obj = $(this);
		var url = $(this).attr('href');
		var tip = $(this).data('tip');
		var jump = $(this).data('jump');
		var param = {jump : jump};
		layer.prompt({title: tip, formType: 2}, function(text, index){
			layer.close(index);
			param.prompttext = text;
			$.get(url, param, function(ret){
				if(ret.code == 1){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 1, data: ret.data, url: ret.url});
				}else if(ret.code == 0){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 2, data: ret.data, url: ret.url});
				}
				try {
					var re = afterConfirm(ret);
					if (re != true) {
						return;
					};
				} catch (err) {

				}
			}, 'json');
		}, function(){
		});
	});
}

//直接ajax操作
var ajaxSend_list = $('a.J_ajaxSend');
if(ajaxSend_list.length){
	$('a.J_ajaxSend').on('click', function(e){
		e.preventDefault();
		layer.load(0, {shade: [0.1,'#fff'], time: 1000});
		var url = $(this).attr('href');
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: "",
			success: function(ret){
				if(ret.code == 1){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 1, data: ret.data, url: ret.url}, function(){
						layer.closeAll();
						Wei.Util.readyToJump(ret);
					});
				}else if(ret.code == 0){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 2, data: ret.data, url: ret.url}, function(){
						layer.closeAll();
						Wei.Util.readyToJump(ret);
					});
				}
			},
			error: function(e){
				console.log(e.status);
				console.log(e.responseText);
			}
		});
	});
}

//ajax弹框
var ajaxDialog_list = $('a.J_ajaxDialog');
if(ajaxDialog_list.length){
	$('a.J_ajaxDialog').on('click', function(e){
		e.preventDefault();
		var id = $(this).attr('id');
		var url = $(this).attr('href');
		var title = $(this).data('tip');
		var para = $(this).data('para');
		var param;
		if(para){
			param = {para : para};
		}
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: "",
			success: function(ret){
				if(ret.code == 1){
					var data = ret.data;
					var html = data.html;
					layer.open({
						type: 1,
						closeBtn: 1,
						shift: 2,
						area: ['auto'],
						title: title,
						content: html
					});
				}else if(ret.code == 0){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 2, data: ret.data, url: ret.url}, function(){
						layer.closeAll();
						Wei.Util.readyToJump(ret);
					});
				}
			},
			error: function(e){
				console.log(e.status);
				console.log(e.responseText);
			}
		});
	});
}

//ajax弹框
var layerDialog_list = $('a.J_layerDialog');
if (layerDialog_list.length) {
	$('a.J_layerDialog').on('click', function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		var title = $(this).data('tip');
		var para = $(this).data('para');
		var param;
		if(para){
			param = {para : para};
		}
		$.get(url, param, function(data){
			var dataset;
			try{
				dataset = $.parseJSON(data);
				if(dataset.code == 0){
					Wei.Util.wapTipNew({msg: dataset.msg, icon: 2, data: dataset.data, url: dataset.url});
				}
			}catch(e){
				layer.open({
					type: 1,
					closeBtn: 1,
					shift: 2,
					area: ['auto'],
					title: title,
					content: data
				});
			}
		}, 'text');
	});
}
//ajax弹框for pc
var layerDialog_list = $('a.J_layerPcDialog');
if(layerDialog_list.length){
	$('a.J_layerPcDialog').on('click', function(e){
		e.preventDefault();

		var url = $(this).attr('href');
		var title = $(this).data('tip');
		var para = $(this).data('para');
		var param;
		if(para){
			param = {para : para};
		}
		$.get(url, param, function(data){
			var dataset;
			try{
				dataset = $.parseJSON(data);
				if(dataset.code == 0){
					Wei.Util.wapTipNew({msg: dataset.msg, icon: 2, data: dataset.data, url: dataset.url});
				}
			}catch(e){
				layer.open({
					type: 1,
					closeBtn: 1,
					shift: 2,
					maxWidth: 980,
					title: title,
					content: data
				});
			}
		}, 'text');
	});
}
//ajax弹框for pcnew
var layerDialog_list = $('a.J_layerPcnewDialog');
if(layerDialog_list.length){
	$('a.J_layerPcnewDialog').on('click', function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		var title = $(this).data('tip');
		var para = $(this).data('para');
		var param;
		if(para){
			param = {para : para};
		}
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: "",
			success: function(ret){
				console.log('ajax请求返回结果：', ret)
				if(ret.code == 1){
					var data = ret.data;
					var html = data.html;
					layer.open({
						type: 1,
						closeBtn: 1,
						shift: 2,
						area: ['auto'],
						title: title,
						content: html
					});
				}else if(ret.code == 0){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 2, data: ret.data, url: ret.url}, function(){
						layer.closeAll();
						Wei.Util.readyToJump(ret);
					});
				}
			},
			error: function(e){
				console.log(e.status);
				console.log(e.responseText);
			}
		});
	});
}
//ajax弹框for wap
var layerDialog_list = $('a.J_layerWapDialog');
if(layerDialog_list.length){
	$('a.J_layerWapDialog').on('click', function(e){
		e.preventDefault();

		var url = $(this).attr('href');
		var title = $(this).data('tip');
		var para = $(this).data('para');
		var param;
		if(para){
			param = {para : para};
		}
		$.get(url, param, function(data){
			var dataset;
			try{
				dataset = $.parseJSON(data);
				if(dataset.code == 0){
					Wei.Util.wapTipNew({msg: dataset.msg, icon: 2, data: dataset.data, url: dataset.url});
				}
			}catch(e){
				layer.open({
					type: 1,
					closeBtn: 1,
					shift: 2,
					area: ['auto'],
					title: '',
					content: data
				});
			}
		}, 'text');
	});
}
var layerDialog_list = $('a.J_layerWapnewDialog');
if(layerDialog_list.length){
	$('a.J_layerWapnewDialog').on('click', function(e){
		e.preventDefault();
		var url = $(this).attr('href');
		var title = $(this).data('tip');
		var full = $(this).data('full');
		$.ajax({
			type: "POST",
			dataType: "JSON",
			url: url,
			data: "",
			success: function(ret){
				if(ret.code == 1){
					var data = ret.data;
					var html = data.html;
					if(full == 1){
						layer.open({
							type: 1,
							closeBtn: 1,
							shift: 2,
							area: ['100%', '100%'],
							title: title,
							content: html
						});
					}else{
						layer.open({
							type: 1,
							closeBtn: 1,
							shift: 2,
							area: ['auto'],
							title: title,
							content: html
						});
					}
				}else if(ret.code == 0){
					Wei.Util.wapTipNew({msg: ret.msg, icon: 2, data: ret.data, url: ret.url}, function(){
						layer.closeAll();
						Wei.Util.readyToJump(ret);
					});
				}
			},
			error: function(e){
				console.log(e.status);
				console.log(e.responseText);
			}
		});
	});
}

//一些跳转
$(document).on('click', '.J_url', function(e){
	e.preventDefault();
	window.location = $(this).data('url');
	e.stopPropagation();
});
$('.J_excel').on('click', function(e){
	e.preventDefault();
	var btn = $(this),
		form = btn.parents('form.J_ajaxForm');

	var url = btn.data('action');
	form.attr('action', url);
	form.submit();
});
$('.J_searchForExcel').on('click', function(e){
	e.preventDefault();
	var btn = $(this),
		form = btn.parents('form.J_ajaxForm');

	var actionName = btn.data('action');
	console.log('actionName:', actionName);
	form.find("input[name='a']").val(actionName);
	form.submit();
});
//上传按钮美化
$('.J_filebox').on('change', "input[type='file']",function(){
    var filePath=$(this).val();
    var arr=filePath.split('\\');
    var fileName=arr[arr.length-1];
    $(".fileNameShow").html(fileName);
});