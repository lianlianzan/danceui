
## 前言
DanceWeui V2.0.18基于weui1.1.3和jquery等众多开源项目参考而成的，收集整理了上百个组件开发而成，主要用于微信手机网站开发，具有轻量化、可定制的优势。

## 产品
官方网站：http://www.danceui.com
PHP框架：http://www.dancephp.com
H5版本的DanceUI组件库地址：https://gitee.com/lianlianzan/danceui
uniapp版本的DanceUI组件库地址：https://gitee.com/lianlianzan/danceui-uniapp
uniapp官方下载地址：https://ext.dcloud.net.cn/plugin?id=10700

uucms-mall多用户分销商城_PC_APP_小程序_全平台：https://gitee.com/lianlianzan/uucms-mall

## 联系我
欢迎技术探讨，部署安装及定制开发，还望留步给个star

```
邮箱：13040@qq.com
微信：zhaojungg
公众号：连连赞
QQ群交流：1311641/3359828
```

## 捐赠支持
开源项目不易，若此项目能得到你的青睐，可以捐赠支持作者持续开发与维护，感谢所有支持开源的朋友。

| ![Image text](http://img.uucms.com/zjalipay-sm.jpg) |![Image text](http://img.uucms.com/zjwxpay-sm.jpg) |
| ---- | ---- |

## 更新日志

```
2023年1月31日 v2.1.0
完善新闻模板
```

```
2022年10月31日 v2.1.0
完善折叠面板、轮播图等功能
```

```
2022年10月1日 v2.1.0
完善商城模板
```

```
2022年8月1日 v2.1.0
完善搜索功能
```

```
2022年6月1日 v2.0.6
完善头条标题、收货地址、登录模板等扩展模块
```

```
2022年5月1日 v2.0.1
完善按钮、标签、徽章等基础功能
```

```
2018年5月1日 v1.0.1
基础功能实现
```